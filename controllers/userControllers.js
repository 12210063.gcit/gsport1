const user = require('./../model/userModel')
exports.getAllUsers =async(req, res, next)=>{
    try{
        const users = await user.find()
        res.status(200).json({data: users, status: 'success'});
    }catch(err){
        res.status(500).json({error: err.messaage});
    }
}

exports.createUser = async(req, res) =>{
    try{
        const user = await User.create(req.body);
        console.log(req.body.name)
        res.json({data: user, status: 'success'});
    }catch(err){
        res.status(500).json({error: err.messaage});
    }
}

exports.getUser = async (req, res)=>{
    try{
        const user = await User.findByID(req.params.ID);
        res.json({data: User, status: 'success'});
    }catch(err){
        res.status(500).json({error: err.messaage});
    }
}

exports.updateUser = async (req, res)=>{
    try{
        const user = await User.findByIDAndUpdate(req.params.ID, req.body);
        res.json({data: User, status: 'success'});
    }catch(err){
        res.status(500).json({error: err.messaage});
    }
}

exports.deleteUser = async (req, res)=>{
    try{
        const user = await User.findByIDAndDelete(req.params.ID);
        res.json({data: User, status: 'success'});
    }catch(err){
        res.status(500).json({error: err.messaage});
    }
}