const path = require('path')

/* Landing PAGE */

exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'landing.html'))
};