const mongoose = require('mongoose')
const validator = require('validator')
const userSchema = new mongoose.Schema({
    enrollment: {
        type: Number,
        require: true,
        unique: true

    },

    name: {
        type: String,
        require: [true, 'Please tell us your name'],
    },
    email: {
        type: String,
        require: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg'
    },
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user',
    },
    password: {
        type: String,
        require: [true, 'please provide a password!'],
        minlength: 8,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})
const User = mongoose.model('User', userSchema)
module.exports = User